CREATE DATABASE GestionYsistemas

use GestionYsistemas


CREATE TABLE dbo.Cliente  
   (CodCliente varchar(10) PRIMARY KEY NOT NULL,  
   NombreCompleto varchar(200) NOT NULL,  
   NombreCorto varchar(40) NOT NULL,  
   Abreviatura varchar(40) NOT NULL,  
   Ruc varchar(11) NOT NULL,  
   Estado char(1) NOT NULL,  
   GrupoFacturacion varchar(100) NOT NULL,  
   InactivoDesde datetime NOT NULL,  
   CodigoSAP varchar(20) NOT NULL
   )
GO  

select * from Cliente

INSERT INTO Cliente VALUES('1','JORGE LUIS','JORGE','JL','1031609986','1','HABILITADO','10/05/2022','32435634');


CREATE PROCEDURE RegistrarCliente  
   @CodCliente varchar(10),  
   @NombreCompleto varchar(200) ,  
   @NombreCorto varchar(40),  
   @Abreviatura varchar(40),  
   @Ruc varchar(11),  
   @Estado char(1),  
   @GrupoFacturacion varchar(100) ,  
   @InactivoDesde datetime ,  
   @CodigoSAP varchar(20)
AS 
INSERT INTO Cliente (CodCliente,NombreCompleto,NombreCorto,Abreviatura,Ruc,Estado,GrupoFacturacion,InactivoDesde,CodigoSAP) VALUES
(@CodCliente,@NombreCompleto,@NombreCorto,@Abreviatura,@Ruc,@Estado,@GrupoFacturacion,@InactivoDesde,@CodigoSAP)
GO

CREATE PROCEDURE ActualizarCliente  
   @CodCliente varchar(10),  
   @NombreCompleto varchar(200) ,  
   @NombreCorto varchar(40),  
   @Abreviatura varchar(40),  
   @Ruc varchar(11),  
   @Estado char(1),  
   @GrupoFacturacion varchar(100) ,  
   @InactivoDesde datetime ,  
   @CodigoSAP varchar(20)
AS 
UPDATE Cliente SET  
		NombreCompleto=@NombreCompleto,
		NombreCorto=@NombreCorto,
		Abreviatura=@Abreviatura,
		Ruc=@Ruc,
		Estado=@Estado,
		GrupoFacturacion=@GrupoFacturacion,
        InactivoDesde=@InactivoDesde,
		CodigoSAP=@CodigoSAP
		WHERE CodCliente=@CodCliente
GO

CREATE PROCEDURE EliminarCliente  
   @CodCliente varchar(10)
AS 
Delete from Cliente where CodCliente=@CodCliente
GO

CREATE PROCEDURE ListarCliente  
AS 
select * from Cliente
GO




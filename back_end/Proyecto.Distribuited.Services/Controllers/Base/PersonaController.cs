﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Proyecto.Application.Base.Services.Persona;
using Proyecto.Application.Dto.Base.Persona;

namespace Proyecto.Distribuited.Services.Controllers.Base
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PersonaController : Controller
    {
        private readonly IPersonaAppService _iPersonaAppService;

        public PersonaController(IPersonaAppService iPersonaAppService)
        {
            _iPersonaAppService = iPersonaAppService;
        }

        [HttpPost("RegistrarCliente")]
        public IActionResult RegistrarAlumno([FromBody] ClienteDto persona)
        {
            return Ok(_iPersonaAppService.RegistrarCliente(persona));
        }
        [HttpPost("EliminarCliente")]
        public IActionResult EliminarCliente([FromBody] ClienteDto persona)
        {
            return Ok(_iPersonaAppService.EliminarCliente(persona));
        }
        [HttpPost("ActualizarCliente")]
        public IActionResult ActualizarCliente([FromBody] ClienteDto persona)
        {
            return Ok(_iPersonaAppService.ActualizarCliente(persona));
        }
        [HttpGet("ListarCliente")]
        public IActionResult ListarCliente()
        {
            return Ok(_iPersonaAppService.ListarCliente());
        }

        //-------------------Conexion con S3 Amazon--------------------------------------------

        //[HttpPost("ConexionS3")]
        //public async Task<IActionResult> Registrar([FromForm] string dato, IFormCollection formData)
        //{
        //    try
        //    {
        //        var request = new PutObjectRequest
        //        {
        //            InputStream = ms,
        //            Key = ruta.nombrekey,
        //            ContentType = file.ContentType,
        //            CalculateContentMD5Header = true,
        //            BucketName = _xsS3Client.ObtenerBucketName(),
        //            Headers = { ContentDisposition = "attachment; filename*=UTF-8''" + Uri.EscapeDataString(ruta.nombre) }
        //        };
        //        var response = await _xsS3Client.ObtenerClienteS3().PutObjectAsync(request);
        //        switch (response.HttpStatusCode)
        //        {
        //            case HttpStatusCode.OK:
        //                var fullPath = $"{_xsS3Client.ObtenerBucketName()}:{ruta.nombrekey}";
        //                ruta.direccion = fullPath;
        //                return Ok(_iArchivoAppService.Registrar(ruta));
        //            default:
        //                return StatusCode((int)response.HttpStatusCode, $"{response.ResponseMetadata}");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(500, $"Internal server error: {ex}");
        //    }
        //}
    }
}

﻿using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;
using Proyecto.Infraestructure.Cross.Cuting.XsHelpers;

namespace Proyecto.Distribuited.Services.Services
{
    public class XsFileService
    {

        public string? SaveFile(IFormFile file, string? subDirectory)
        {
            string? fileNameSaved = null;

            subDirectory ??= string.Empty;
            var target = Path.Combine(Directory.GetCurrentDirectory(), "Files", subDirectory);

            if (!Directory.Exists(target))
                Directory.CreateDirectory(target);

            if (file.Length <= 0) return fileNameSaved;
            string prefix = XsHelper.RandomString(6);
            string currentDateTime = XsHelper.CurrentDateTimeString();
            string extension = Path.GetExtension(file.FileName);
            var filePath = Path.Combine(target, prefix + currentDateTime + extension);
            fileNameSaved = prefix + currentDateTime + extension;
            using var stream = new FileStream(filePath, FileMode.Create);
            file.CopyTo(stream);

            return fileNameSaved;
        }
        public string? SaveFiles(List<IFormFile> files, string? subDirectory)
        {
            string? fileNameSaved = null;

            subDirectory ??= string.Empty;
            var target = Path.Combine(Directory.GetCurrentDirectory(), "Files", subDirectory);

            if (!Directory.Exists(target))
                Directory.CreateDirectory(target);


            files.ForEach(async file =>
            {
                if (file.Length <= 0) return;
                string currentDateTime = XsHelper.CurrentDateTimeString();
                string extension = Path.GetExtension(file.FileName);
                var filePath = Path.Combine(target, currentDateTime + extension);
                fileNameSaved = currentDateTime + extension;
                await using var stream = new FileStream(filePath, FileMode.Create);
                await file.CopyToAsync(stream);
            });

            return fileNameSaved;
        }

        public (string fileType, byte[] archiveData, string archiveName) DownloadFile(string subDirectory, string fileName)
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), "Files", subDirectory, fileName);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                stream.CopyTo(memory);
            }
            memory.Position = 0;

            return (GetContentType(path), memory.ToArray(), Path.GetFileName(path));
        }

        private static string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private static Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
    }
}

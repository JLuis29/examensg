﻿using System.Collections.Generic;
using AutoMapper;

using Proyecto.Application.Dto.Base.Persona;

using Proyecto.Domain.Entities.Base.Models;

namespace Proyecto.Distribuited.Services
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {

            CreateMap<ClienteModel, ClienteDto>().ReverseMap();

            CreateMap<NotasModel, NotasDto>().ReverseMap();



        }
    }
}

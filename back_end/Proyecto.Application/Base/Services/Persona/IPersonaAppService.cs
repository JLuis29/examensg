﻿using System.Collections.Generic;
using System.Reactive;
using Proyecto.Application.Dto.Base.Generico;
using Proyecto.Application.Dto.Base.Persona;
using Proyecto.Domain.Entities.Base.Models;
using Proyecto.Infraestructure.Cross.Cuting.XsEfCore;

namespace Proyecto.Application.Base.Services.Persona
{
    public interface IPersonaAppService

    {
        XsRestServiceResponse<ResultadoDto> RegistrarCliente(ClienteDto sistema);
        XsRestServiceResponse<ResultadoDto> EliminarCliente(ClienteDto sistema);
        XsRestServiceResponse<ResultadoDto> ActualizarCliente(ClienteDto sistema);
        XsRestServiceResponse<List<ClienteModel>> ListarCliente();
    }
}

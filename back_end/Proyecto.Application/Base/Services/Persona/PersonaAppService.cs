﻿using System;
using System.Collections.Generic;
using System.Reactive;
using AutoMapper;
using Proyecto.Application.Dto.Base.Generico;
using Proyecto.Application.Dto.Base.Persona;
using Proyecto.Domain.Base;
using Proyecto.Domain.Entities.Base.Models;
using Proyecto.Infraestructure.Cross.Cuting.XsEfCore;
using Amazon.S3.Model;
namespace Proyecto.Application.Base.Services.Persona
{
    public class PersonaAppService : IPersonaAppService
    {
        private readonly IPersonaRepository _iPersonaRepository;
        private readonly IMapper _mapper;

        public PersonaAppService(
            IPersonaRepository iPersonaRepository,
            IMapper mapper
            )
        {
            _iPersonaRepository = iPersonaRepository;
            _mapper = mapper;
        }
        
       
        
        public XsRestServiceResponse<ResultadoDto> RegistrarCliente(ClienteDto persona)
        {
            XsRestServiceResponse<ResultadoDto> res = new();

            try
            {
                
                ClienteModel model = _mapper.Map<ClienteModel>(persona);
                XsDataAccessResponse<ResultadoDto> respuesta = _iPersonaRepository.RegistrarCliente(model);

                respuesta.Estado = true;
                res.Content = respuesta.Content;
            }
            catch (Exception ex)
            {
                res.Estado = false;
                res.MensajeError = ex.Message;
            }

            return res;
        }

     
        

        public XsRestServiceResponse<ResultadoDto> EliminarCliente(ClienteDto persona)
        {
            XsRestServiceResponse<ResultadoDto> res = new();

            try
            {

                ClienteModel model = _mapper.Map<ClienteModel>(persona);
                XsDataAccessResponse<ResultadoDto> respuesta = _iPersonaRepository.EliminarCliente(model);

                respuesta.Estado = true;
                res.Content = respuesta.Content;
            }
            catch (Exception ex)
            {
                res.Estado = false;
                res.MensajeError = ex.Message;
            }

            return res;
        }

        

      
        
        public XsRestServiceResponse<ResultadoDto> ActualizarCliente(ClienteDto persona)
        {
            XsRestServiceResponse<ResultadoDto> res = new();

            try
            {
                ClienteModel model = _mapper.Map<ClienteModel>(persona);
                XsDataAccessResponse<ResultadoDto> respuesta = _iPersonaRepository.ActualizarCliente(model);

                respuesta.Estado = true;
                res.Content = respuesta.Content;

            }
            catch (Exception ex)
            {
                res.Estado = false;
                res.MensajeError = ex.Message;
            }

            return res;
        }

        public XsRestServiceResponse<List<ClienteModel>> ListarCliente()
        {
            XsRestServiceResponse<List<ClienteModel>> res = new();

            XsDataAccessResponse<List<ClienteModel>> obtenerListado = _iPersonaRepository.ListarCliente();
            if (!obtenerListado.Estado) return obtenerListado.GetRestServiceResponse();
            res.Content = obtenerListado.Content;
            return res;
        }


      


    }
}

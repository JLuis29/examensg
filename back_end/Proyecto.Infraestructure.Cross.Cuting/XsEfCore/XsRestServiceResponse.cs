﻿using System;

namespace Proyecto.Infraestructure.Cross.Cuting.XsEfCore
{
    public class XsRestServiceResponse<T>
    {
        public string? MensajeError { get; set; }
        public bool Estado { get; set; }
        public T? Content { get; set; }

        public XsRestServiceResponse()
        {
            Estado = true;
        }

        private static string GetMensajeDeError(Exception ex)
        {
            return "\nMensaje:" + ex.Message + "\n Stacktrace: " + (ex.StackTrace ?? "");
        }

        public void SetException(Exception ex)
        {
            Estado = false;
            MensajeError = GetMensajeDeError(ex);
        }
        public void SetMensajeError(string mensaje)
        {
            Estado = false;
            MensajeError = mensaje;
        }
    }
}

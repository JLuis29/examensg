﻿using System;

namespace Proyecto.Infraestructure.Cross.Cuting.XsEfCore
{
    public class XsDataAccessResponse<T>
    {
        public int Count;

        public string? MensajeError { get; set; }
        public bool Estado { get; set; }
        public T? Content { get; set; }

        public XsDataAccessResponse()
        {
            Estado = true;
        }

        private static string GetMensajeDeError(Exception ex)
        {
            return "\nMensaje:" + ex.Message + "\n Stacktrace: " + (ex.StackTrace ?? "Stacktrace es null");
        }

        public void SetException(Exception ex)
        {
            Estado = false;
            MensajeError = GetMensajeDeError(ex);
        }
        public void SetMensajeError(string mensaje)
        {
            Estado = false;
            MensajeError = mensaje;
        }

        public XsRestServiceResponse<T> GetRestServiceResponse()
        {
            return new XsRestServiceResponse<T> { Content = Content, Estado = Estado, MensajeError = MensajeError };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Microsoft.Extensions.Configuration;

namespace Proyecto.Infraestructure.Cross.Cuting.Correo
{
    public class CorreoOpcional
    {
        /***************************************************************************************/
        // Variables publicas
        public IConfiguration Configuration { get; }

        public string MailDestino { get; set; }
        public string Asunto { get; set; }
        public string Cuerpo { get; set; }
        /***************************************************************************************/
        public CorreoOpcional(IConfiguration iConfiguration) { 
            Configuration = iConfiguration;
        }

        /***************************************************************************************/
        public bool EnviarCorreoVarios()
        {
            try
            {
                string smtpHost = Configuration["Correo:Email_Host"];
                var smtpPort = int.Parse(Configuration["Correo:Email_Port"]);
                string smtpUserName = Configuration["Correo:Email_UserName"];
                string smtpPassword = Configuration["Correo:Email_Password"];
                SmtpClient client = new(smtpHost)
                {
                    Port = smtpPort,
                    Credentials = new NetworkCredential(smtpUserName, smtpPassword),
                    EnableSsl = true
                };

                List<string> listaCorreos = MailDestino.Split(Convert.ToChar(@";")).ToList();
                MailMessage objMsg = new();
                foreach (var enviarA in listaCorreos.Where(enviarA => !string.IsNullOrWhiteSpace(enviarA)))
                {
                    objMsg.To.Add(new MailAddress(enviarA));
                }
                objMsg.From = new MailAddress(smtpUserName);
                objMsg.IsBodyHtml = true;
                objMsg.Subject = Asunto;
                objMsg.Body = Cuerpo;
                client.Send(objMsg);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool EnviarCorreo()
        {
            try
            {
                string smtpHost = Configuration["Correo:Email_Host"];
                var smtpPort = int.Parse(Configuration["Correo:Email_Port"]);
                string smtpUserName = Configuration["Correo:Email_UserName"];
                string smtpPassword = Configuration["Correo:Email_Password"];
                SmtpClient client = new()
                {
                    Host=smtpHost,
                    Port = smtpPort,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(smtpUserName, smtpPassword)
                };
                string id = $"<{Guid.NewGuid().ToString()}@turismomaster.com>";
                MailMessage objMsg = new(smtpUserName, MailDestino)
                {
                    IsBodyHtml = true,
                    Subject = Asunto,
                    Body = Cuerpo,
                    From = new MailAddress(smtpUserName)
                };
                objMsg.Headers.Add("message-id", id);
                client.Send(objMsg);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //public string ObtenerPlantilla(string busqueda)
        //{
        //    string plantilla = "";
        //    try
        //    {
        //        DirectoryInfo d = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/PlantillaCorreo"));
        //        FileInfo[] Plantillas = d.GetFiles("*.html");
        //        string NombrePlantilla = "";
        //        foreach (FileInfo p in Plantillas)
        //        {
        //            string nombreCompleto = Path.GetFileNameWithoutExtension(p.Name);
        //            string[] div = nombreCompleto.Split('_');

        //            if (div.Length != 2) continue;

        //            if (busqueda.Equals(nombreCompleto, StringComparison.OrdinalIgnoreCase)
        //                || busqueda.Equals(div[0], StringComparison.OrdinalIgnoreCase)
        //                || busqueda.Equals(div[1], StringComparison.OrdinalIgnoreCase))
        //            {
        //                NombrePlantilla = p.Name;
        //                break;
        //            }
        //        }

        //        if (NombrePlantilla.Trim().Length > 0)
        //        {
        //            string rutaPlantilla = Path.Combine(HttpContext.Current.Server.MapPath("~/PlantillaCorreo"), NombrePlantilla);

        //            System.IO.StreamReader file = new System.IO.StreamReader(rutaPlantilla);

        //            string line = "";

        //            while ((line = file.ReadLine()) != null)
        //            {
        //                plantilla = plantilla + " " + line;
        //            }
        //            file.Close();

        //            string url_dominio = ConfigurationManager.AppSettings["UrlDominio"].ToString();
        //            plantilla = plantilla.Replace("[URL_DOMINIO]", url_dominio);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
                
        //    }
        //    return plantilla;
        //}
    }
}

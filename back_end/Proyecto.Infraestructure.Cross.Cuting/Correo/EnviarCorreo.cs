﻿using System;
using System.Net;
using System.Net.Mail;
using Microsoft.Extensions.Configuration;

namespace Proyecto.Infraestructure.Cross.Cuting.Correo
{
    
    public  class EnviarCorreo
    {
        private IConfiguration Configuration { get; }
        private readonly string _nameGmail;
        private readonly string _emailGmail;
        private readonly string _passGmail;
        private readonly string _hostGmail;
        private readonly string _portGmail;
        public EnviarCorreo(IConfiguration iConfiguration)
        {
            Configuration = iConfiguration;
            _nameGmail = "Turismo Master";
            _emailGmail =Configuration["Correo:Email_UserName"];
            _passGmail = Configuration["Correo:Email_Password"];
            _hostGmail = Configuration["Correo:Email_Host"];
            _portGmail = Configuration["Correo:Email_Port"];
        }
        private bool GmailDriver(string asunto, string? correoDestino, string mensaje)
        {
            try
            {
                var senderEmail = new MailAddress(_emailGmail, _nameGmail);

                var receiverEmail = new MailAddress(correoDestino, correoDestino + " -");

                var smtp = new SmtpClient
                {
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(senderEmail.Address, _passGmail),
                    Host = _hostGmail,
                    Port = short.Parse(_portGmail),
                    DeliveryMethod = SmtpDeliveryMethod.Network

                };

                using var mess = new MailMessage(senderEmail, receiverEmail)
                {
                    Subject = asunto,
                    Body = mensaje,
                    IsBodyHtml = true
                };
                mess.IsBodyHtml = true;
                smtp.Send(mess);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex);
            }
            return false;
        }
        public bool SendMail(string asunto, string? correoDestino, string mensaje)
        {
            var send = false;

            try
            {
                send = GmailDriver(asunto, correoDestino, mensaje);
                return send;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return send;
        }
    }
}

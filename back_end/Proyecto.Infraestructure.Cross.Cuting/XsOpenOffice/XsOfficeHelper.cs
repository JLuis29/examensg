﻿using System;
using System.Drawing;
using System.Globalization;
using OfficeOpenXml.Style;

namespace Proyecto.Infraestructure.Cross.Cuting.XsOpenOffice
{
    public static class XsOfficeHelper
    {

        public static int XsOfficeAgregarFila(dynamic document, dynamic tabla, int filaActual)
        {
            if (document.GetType().Name == "ExcelWorksheet")
            {
                return filaActual + 1;
            }

            if (document.GetType().Name != "DocX") return 0;
            if (filaActual > 0)
            {
                tabla.InsertRow();
            }

            return filaActual + 1;

        }
        public static void XsOfficeConfigurarColumna(dynamic document, dynamic tabla, int columnaIndex, int ancho, bool textWrapped)
        {

            if (document.GetType().Name == "ExcelWorksheet")
            {
                document.Column(columnaIndex).Width = ancho;
                document.Column(columnaIndex).Style.WrapText = true;
            }
            if (document.GetType().Name == "DocX")
            {
                //tabla.SetColumnWidth(columnaIndex - 1, Convert.ToDouble(ancho/10));
            }

        }

        public static void XsOfficeMergeHorizontal(dynamic document, dynamic tabla, dynamic row, dynamic colIni, dynamic colFin)
        {
            if (document.GetType().Name == "ExcelWorksheet")
            {
                string rango = colIni + row + ":" + colFin + row;
                if (colIni != colFin)
                    document.Cells[rango].Merge = true;
            }

            if (document.GetType().Name != "DocX") return;
            int mcolIni = ColumnaExcel2Word(colIni);
            int mcolFin = ColumnaExcel2Word(colFin);
            if (mcolIni != mcolFin)
                tabla.Rows[row - 1].MergeCells(mcolIni, mcolFin);
        }
        public static void XsOfficeMergeVertical(dynamic document, dynamic tabla, dynamic rowIni, dynamic rowFin, dynamic columna)
        {
            if (document.GetType().Name == "ExcelWorksheet")
            {
                string rango = columna + rowIni + ":" + columna + rowFin;
                if (rowIni != rowFin)
                    document.Cells[rango].Merge = true;
            }
            if (document.GetType().Name == "DocX")
            {
                //int mFilaIni = Convert.ToInt32(rowIni);
                //int mFilaFin = Convert.ToInt32(rowFin);
                //int mColumna = ColumnaExcel2Word(columna);
                //if (mFilaIni != mFilaFin)
                //    tabla.MergeCellsInColumn(mColumna, mFilaIni, mFilaFin);
            }
        }
        public static void XsOfficeSetValue(dynamic document, dynamic table, dynamic row, dynamic colIni, dynamic colFin, string value, string dataType = "Text", string format = "")
        {
            if (document.GetType().Name == "ExcelWorksheet")
            {
                string rango = colIni + row + ":" + colFin + row;

                switch (dataType)
                {
                    case "Integer":
                        if (string.IsNullOrEmpty(value)) value = "0";
                        value = Math.Round(Convert.ToDecimal(value)).ToString(CultureInfo.CurrentCulture);
                        if (int.TryParse(value, out var valorEntero))
                            document.Cells[rango].Value = valorEntero;
                        break;
                    case "Decimal":
                        if (decimal.TryParse(value, out var valorDecimal))
                            document.Cells[rango].Value = valorDecimal;
                        break;
                    default:
                        document.Cells[rango].Value = value;
                        break;
                }

                document.Cells[rango].Style.WrapText = true;
                if (!format.Equals("")) document.Cells[rango].Style.Numberformat.Format = format;

            }
            if (document.GetType().Name == "DocX")
            {
                //int columna = ColumnaExcel2Word(colIni);
                //table.Rows[row - 1].Cells[columna].Paragraphs[0].Append(value);
                //table.Rows[row - 1].Cells[columna].Paragraphs[0].Alignment = Alignment.left;
            }
        }
        public static dynamic XsOfficeGetText(dynamic document, dynamic table, dynamic row, dynamic colIni, dynamic colFin)
        {

            if (document.GetType().Name == "ExcelWorksheet")
            {
                string rango = colIni + row + ":" + colFin + row;
                return document.Cells[rango].Text;
            }

            if (document.GetType().Name != "DocX") return "";
            int columna = ColumnaExcel2Word(colIni);
            return table.Rows[row - 1].Cells[columna].Paragraphs[0].Text;
        }
        public static void XsOfficeSetHiperlink(dynamic document, dynamic tabla, dynamic row, dynamic colIni, dynamic colFin, string link, string tipoEvidencia)
        {


            if (document.GetType().Name == "ExcelWorksheet")
            {
                try
                {
                    string rango = colIni + row + ":" + colFin + row;
                    document.Cells[rango].Style.WrapText = true;
                    document.Cells[rango].Formula = "HYPERLINK(\"" + link + "\",\"" + tipoEvidencia + "\")";
                    document.Cells[rango].Style.Font.Color.SetColor(Color.Blue);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (document.GetType().Name == "DocX")
            {
                //var h = document.AddHyperlink(tipoEvidencia, new Uri(link));
                //tabla.Rows[row - 1].Cells[ColumnaExcel2Word(colIni)].Paragraphs[0].AppendHyperlink(h).Color(Color.Blue).UnderlineStyle(UnderlineStyle.singleLine).Italic();
            }
        }
        public static void XsOfficeConfigurarEstiloRango(dynamic document, dynamic tabla, dynamic rowIni, dynamic rowFin, dynamic colIni, dynamic colFin, Color fontColor, bool isBold, dynamic backgroundPatternType, Color backgrounColor)
        {
            if (document.GetType().Name == "ExcelWorksheet")
            {
                string rangoId = colIni + rowIni + ":" + colFin + rowFin;
                dynamic rango = document.Cells[rangoId];
                rango.Style.Font.Color.SetColor(fontColor);
                rango.Style.Font.Bold = isBold;
                rango.Style.Fill.PatternType = backgroundPatternType;
                rango.Style.Fill.BackgroundColor.SetColor(backgrounColor);
            }

            if (document.GetType().Name != "DocX") return;
            tabla.Rows[rowIni - 1].Cells[ColumnaExcel2Word(colIni)].FillColor = backgrounColor;
            tabla.Rows[rowIni - 1].Cells[ColumnaExcel2Word(colIni)].Paragraphs[0].Color(fontColor);
        }

        public static long ColumnNumber(string columnName)
        {
            char[] chars = columnName.ToUpper().ToCharArray();

            return (long)Math.Pow(26, chars.Length - 1) *
                (Convert.ToInt32(chars[0]) - 64) +
                (chars.Length > 2 ? ColumnNumber(columnName.Substring(1, columnName.Length - 1)) :
                chars.Length == 2 ? Convert.ToInt32(chars[^1]) - 64 : 0);
        }

        public static int ColumnaExcel2Word(string nombreColumna)
        {

            char[] chars = nombreColumna.ToUpper().ToCharArray();

            var number = (long)Math.Pow(26, chars.Length - 1) *
                         (Convert.ToInt32(chars[0]) - 64) +
                         (chars.Length > 2 ? ColumnNumber(nombreColumna.Substring(1, nombreColumna.Length - 1)) :
                             chars.Length == 2 ? Convert.ToInt32(chars[^1]) - 64 : 0);

            return Convert.ToInt32(number - 1);
        }

        public static void XsOfficeCuadricularRango(dynamic document, dynamic filaIni, dynamic filaFin, dynamic columnaIni, dynamic columnaFin, Color borderColor)
        {
            if (document.GetType().Name != "ExcelWorksheet") return;
            var range = document.Cells[columnaIni + filaIni + ":" + columnaFin + filaFin];

            range.Style.Border.Top.Style = ExcelBorderStyle.Dotted;
            range.Style.Border.Top.Color.SetColor(borderColor);

            range.Style.Border.Bottom.Style = ExcelBorderStyle.Dotted;
            range.Style.Border.Bottom.Color.SetColor(borderColor);

            range.Style.Border.Left.Style = ExcelBorderStyle.Dotted;
            range.Style.Border.Left.Color.SetColor(borderColor);

            range.Style.Border.Right.Style = ExcelBorderStyle.Dotted;
            range.Style.Border.Right.Color.SetColor(borderColor);
        }
    }
}

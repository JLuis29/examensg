﻿namespace Proyecto.Infraestructure.Cross.Cuting.XsHelpers
{
    public class XsFileHelper
    {
        public string GetFolderName(string tipo)
        {
            string folder = "Temp";

            if (!string.IsNullOrEmpty(tipo))
            {
                folder = tipo.ToUpper() switch
                {
                    "PERSONA" => "persona",
                    "PROYECTOS" => "proyectos",
                    _ => folder
                };
            }

            return folder;
        }
    }
}

﻿using System;
using System.Linq;

namespace Proyecto.Infraestructure.Cross.Cuting.XsHelpers
{
    public class XsHelper
    {
        public static string CurrentDateTimeString(string format = "yyyyddMMHHmmss")
        {
            return DateTime.Now.ToString(format);
        }

        public static string RandomString(int length)
        {
            Random random = new();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string ValidarEnviarData(string dato)
        {
            if (string.IsNullOrEmpty(dato))
            {
                dato = "null";
            }
            else
            {
                dato = "'" + dato + "'";
            }
            return dato;
        }

        public static string ValidarWhereData(string? dato)
        {
            return dato ??= "";
        }
    }
}

#!/bin/bash

echo "Getting last source code ... "
git pull origin dev


#cd /home/xsdeploy/xs_net_publish/xs-care-backend/Xs.Care.Distribuited.Services

echo "Compiling assemblies"
dotnet publish --configuration Release

DIR="/var/www/xs-care-backend"

if [ -d "$DIR" ]; then
	rm -r /var/www/xs-care-backend  
  	echo "Deleting folder ${DIR} an its content ..."	
fi

echo "Creating folder ${DIR}"
mkdir /var/www/xs-care-backend 

echo "copying assemblies to ${DIR}"
cp -r /home/xsdeploy/dev/xs_net_publish/xs-care-backend/Xs.Care.Distribuited.Services/bin/Release/netcoreapp3.1/publish/. /var/www/xs-care-backend/
 
echo "starting xs.care.backend.service ..."
sudo systemctl stop xs.care.backend.service
sudo systemctl start xs.care.backend.service
sudo systemctl status xs.care.backend.service

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Proyecto.Domain.Entities.Base.Models
{
    public class ClienteModel
    {
        [Key]
        [Column("CodCliente")]
        public string codCliente { get; set; }
        [Column("NombreCompleto")]
        public string nombreCompleto { get; set; }
        [Column("NombreCorto")]
        public string nombreCorto { get; set; }
        [Column("Abreviatura")]
        public string abreviatura { get; set; }
        [Column("Ruc")]
        public string ruc { get; set; }
        [Column("Estado")]
        public string estado { get; set; }
        [Column("GrupoFacturacion")]
        public string grupoFacturacion { get; set; }
        [Column("InactivoDesde")]
        public DateTime? inactivoDesde { get; set; }
        [Column("CodigoSAP")]
        public string codigoSAP { get; set; }

    }
}

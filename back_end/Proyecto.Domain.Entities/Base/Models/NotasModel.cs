﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Proyecto.Domain.Entities.Base.Models
{
    public class NotasModel
    {
        [Key]
        [Column("id_notas")]
        public int? id_notas { get; set; }
        [Column("pc1")]
        public double? pc1 { get; set; }
        [Column("pc2")]
        public double? pc2 { get; set; }
        [Column("pc3")]
        public double? pc3 { get; set; }
        [Column("parcial")]
        public double? parcial { get; set; }
        [Column("final")]
        public double? final { get; set; }
        [Column("promedio_final")]
        public double? promedioFinal { get; set; }
        [Column("id_alumnos_cursos_notas")]
        public int? idAlumnosCursos { get; set; }

    }
}

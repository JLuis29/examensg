﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Proyecto.Domain.Entities.Base.Models
{
    public class PersonaCursosModel
    {
        [Key]
        [Column("id_cursos")]
        public int? idCursos { get; set; }

        [Column("id_alumnos_cursos")]
        public int? idAlumnosCursos { get; set; }
        [Column("nombre")]
        public string? nombre { get; set; }
        [Column("descripcion")]
        public string? descripcion { get; set; }
        [Column("obligatoriedad")]
        public bool? obligatoriedad { get; set; }

        [Column("promedio_final")]
        public decimal? promedioFinal { get; set; }

    }
}

﻿using System;

namespace Proyecto.Application.Dto.Base.Persona
{
    public class PersonaNotasCursoDto
    {
        public int? idAlumnosCursos { get; set; }
        public int? idCursos { get; set; }
    }
}

﻿using System;

namespace Proyecto.Application.Dto.Base.Persona
{
    public class NotasDto
    {

        public int? id_notas { get; set; }
        public double? pc1 { get; set; }
        public double? pc2 { get; set; }
        public double? pc3 { get; set; }
        public double? parcial { get; set; }
        public double? final { get; set; }
        public double? promedioFinal { get; set; }
        public int? idAlumnosCursos { get; set; }


    }
}

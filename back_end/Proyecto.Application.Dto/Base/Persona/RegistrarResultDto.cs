using System.Collections.Generic;

namespace Proyecto.Application.Dto.Base.Persona
{
    public class RegistrarResultDto
    {
        public int Id { get; init; }
        public string? Resultado { get; init; }

        public override string ToString()
        {
            return $"{{ Id = {Id}, resultado = {Resultado} }}";
        }

        public override bool Equals(object? value)
        {
            return (value is RegistrarResultDto type) && EqualityComparer<int>.Default.Equals(type.Id, Id) && EqualityComparer<string>.Default.Equals(type.Resultado, Resultado);
        }

        public override int GetHashCode()
        {
            var num = 0x7a2f0b42;
            num = (-1521134295 * num) + EqualityComparer<int>.Default.GetHashCode(Id);
            return (-1521134295 * num) + EqualityComparer<string>.Default.GetHashCode(Resultado);
        }
    }
}
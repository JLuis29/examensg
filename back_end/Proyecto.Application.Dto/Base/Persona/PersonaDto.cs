﻿using System;

namespace Proyecto.Application.Dto.Base.Persona
{
    public class ClienteDto
    {
        public string codCliente { get; set; }
        public string nombreCompleto { get; set; }
        public string nombreCorto { get; set; }
        public string abreviatura { get; set; }
        public string ruc { get; set; }
        public string estado { get; set; }
        public string grupoFacturacion { get; set; }
        public DateTime? inactivoDesde { get; set; }
        public string codigoSAP { get; set; }
    }
}

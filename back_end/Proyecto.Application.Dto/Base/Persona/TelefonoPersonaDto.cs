﻿using System;

namespace Proyecto.Application.Dto.Base.Persona
{
    public class TelefonoClienteDto
    {
        public int idPersonaTelefono { get; set; }
        public int? idPersona { get; set; }
        public int? idtelefono { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updateAt { get; set; }

    }
}

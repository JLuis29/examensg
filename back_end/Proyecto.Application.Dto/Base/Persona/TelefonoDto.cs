﻿using System;

namespace Proyecto.Application.Dto.Base.Persona
{
    public class TelefonoDto
    {
        public int idTelefono { get; set; }
        public int? operador { get; set; }
        public string? nomOperador { get; set; }
        public string? telefono { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updateAt { get; set; }
        public int? idPersona { get; set; }

    }
}

﻿using System;

namespace Proyecto.Application.Dto.Base.Persona
{
    public class CorreoDto
    {
        public int? idcorreo_marketing { get; set; }
        public string? correo { get; set; }
        public string? nombre { get; set; }
        public string? apellidoP { get; set; }
        public string? apellidoM { get; set; }
        public string? telefono { get; set; }
        public bool? registrado { get; set; }
        public bool? suscripcion { get; set; }
        public string? Usuaccion { get; set; }
    }
}

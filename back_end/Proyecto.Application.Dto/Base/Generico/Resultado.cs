namespace Proyecto.Application.Dto.Base.Generico
{
    public class ResultadoDto
    {
        public string? Resultado { get; set; }
    }
}
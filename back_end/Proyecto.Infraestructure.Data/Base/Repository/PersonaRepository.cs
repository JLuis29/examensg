﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Proyecto.Application.Dto.Base.Generico;
using Proyecto.Application.Dto.Base.Persona;
using Proyecto.Domain.Base;
using Proyecto.Domain.Entities.Base.Models;
using Proyecto.Infraestructure.Cross.Cuting.XsEfCore;
using Proyecto.Infraestructure.Data.Base.Contexts;

namespace Proyecto.Infraestructure.Data.Base.Repository
{
    public class PersonaRepository : IPersonaRepository
    {
        private readonly XsProyectoContext _turismoMContext;
        
        public PersonaRepository(XsProyectoContext dbc)
        {
            _turismoMContext = dbc;
        }
        public XsDataAccessResponse<ResultadoDto> RegistrarCliente(ClienteModel entidad)
        {
            XsDataAccessResponse<ResultadoDto> xsDataAccessResponse = new();
            
            try 
            {
                _turismoMContext.Database.BeginTransaction();
                _turismoMContext.Database.ExecuteSqlInterpolated(
                    @$"INSERT INTO Cliente (CodCliente,NombreCompleto,NombreCorto,Abreviatura,Ruc,Estado,GrupoFacturacion,InactivoDesde,CodigoSAP) VALUES 
                ({entidad.codCliente},{entidad.nombreCompleto},{entidad.nombreCorto},{entidad.abreviatura},{entidad.ruc},{entidad.estado},{entidad.grupoFacturacion},{entidad.inactivoDesde},{entidad.codigoSAP}) ");
                _turismoMContext.Database.CommitTransaction();

                xsDataAccessResponse.Content = new ResultadoDto {Resultado = "Se Registró Correctamente" };
            }
            catch (Exception ex)
            {
                xsDataAccessResponse.SetException(ex);
                xsDataAccessResponse.Content = new ResultadoDto {Resultado = ex.Message };
                _turismoMContext.Database.RollbackTransaction();
            }

            return xsDataAccessResponse;
        }
        public XsDataAccessResponse<ResultadoDto> EliminarCliente(ClienteModel entidad)
        {
            XsDataAccessResponse<ResultadoDto> xsDataAccessResponse = new();
            try
            {
                _turismoMContext.Database.BeginTransaction();
                _turismoMContext.Database.ExecuteSqlInterpolated(
                    @$"Delete from Cliente where CodCliente= {entidad.codCliente}");
                _turismoMContext.Database.CommitTransaction();

                xsDataAccessResponse.Content = new ResultadoDto { Resultado = "Se Elimino Correctamente" };
            }
            catch (Exception ex)
            {
                xsDataAccessResponse.SetException(ex);
                xsDataAccessResponse.Content = new ResultadoDto { Resultado = ex.Message };
                _turismoMContext.Database.RollbackTransaction();
            }

            return xsDataAccessResponse;
        }
        public XsDataAccessResponse<ResultadoDto> ActualizarCliente(ClienteModel entidad)
        {
            XsDataAccessResponse<ResultadoDto> xsDataAccessResponse = new();
            try
            {

                _turismoMContext.Database.BeginTransaction();
                _turismoMContext.Database.ExecuteSqlInterpolated(
                    @$"UPDATE Cliente set NombreCompleto={entidad.nombreCompleto},NombreCorto={entidad.nombreCorto},Abreviatura={entidad.abreviatura},Ruc={entidad.ruc},Estado={entidad.estado},GrupoFacturacion={entidad.grupoFacturacion},
                        InactivoDesde={entidad.inactivoDesde},CodigoSAP={entidad.codigoSAP} where CodCliente={entidad.codCliente}
                   ");
                _turismoMContext.Database.CommitTransaction();

                xsDataAccessResponse.Content = new ResultadoDto {Resultado = "Se actualizo Correctamente" };
            }
            catch (Exception ex)
            {
                xsDataAccessResponse.SetException(ex);
                xsDataAccessResponse.Content = new ResultadoDto {Resultado = ex.Message };
                _turismoMContext.Database.RollbackTransaction();
            }

            return xsDataAccessResponse;
        }
        public XsDataAccessResponse<List<ClienteModel>> ListarCliente()
        {
            XsDataAccessResponse<List<ClienteModel>> xsDataAccessResponse = new();
            try
            {
                StringBuilder sql = new();
                sql.Append("select * from Cliente");
                xsDataAccessResponse.Content = _turismoMContext.ClienteModel.FromSqlRaw(sql.ToString()).ToList();
            }
            catch (Exception ex)
            {
                xsDataAccessResponse.SetException(ex);
            }
            return xsDataAccessResponse;
        }
    }
}

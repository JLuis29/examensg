﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Proyecto.Domain.Entities.Base.Models;

//using Proyecto.Application.Dto.Base.Sistema;
//using Proyecto.Application.Dto.Base.Parametro;

namespace Proyecto.Infraestructure.Data.Base.Contexts
{
    public class XsProyectoContext : DbContext
    {
        public IConfiguration Configuration { get; }
        public XsProyectoContext()
        {

        }

        public XsProyectoContext(DbContextOptions<XsProyectoContext> options, IConfiguration configuration)
            : base(options)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                //optionsBuilder.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"));
                optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            }
        }


        #region TABLAS

        public virtual DbSet<ClienteModel> ClienteModel { get; set; }
        public virtual DbSet<NotasModel> NotasModel { get; set; }

        public virtual DbSet<PersonaCursosModel> PersonaCursosModel { get; set; }
        

        #endregion


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");


            modelBuilder.Entity<ClienteModel>(entity =>
            {
                entity.HasKey(e => e.codCliente)
                    .HasName("cliente_pkey");
                entity.ToTable("Cliente");
            });
            modelBuilder.Entity<NotasModel>(entity =>
            {
                entity.HasKey(e => e.id_notas)
                    .HasName("notas_pkey");
                entity.ToTable("ap_bedon_jorge_alumnos_cursos_notas");
            });
            modelBuilder.Entity<PersonaCursosModel>(entity =>
            {
                entity.HasKey(e => e.idAlumnosCursos)
                    .HasName("alumnos_cursos_pkey");
                entity.ToTable("ap_bedon_jorge_alumnos_cursos");
            });
            

            OnModelCreatingPartial(modelBuilder);
        }

        private void OnModelCreatingPartial(ModelBuilder modelBuilder)
        {
            //throw new NotImplementedException();
        }
    }
}
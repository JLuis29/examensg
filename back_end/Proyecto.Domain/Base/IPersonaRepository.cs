﻿using System.Collections.Generic;
using Proyecto.Application.Dto.Base.Generico;
using Proyecto.Application.Dto.Base.Persona;
using Proyecto.Domain.Entities.Base.Models;
using Proyecto.Infraestructure.Cross.Cuting.XsEfCore;

namespace Proyecto.Domain.Base
{
    public interface IPersonaRepository
    {
        XsDataAccessResponse<ResultadoDto> RegistrarCliente(ClienteModel entidad);
        XsDataAccessResponse<ResultadoDto> EliminarCliente(ClienteModel entidad);
        XsDataAccessResponse<ResultadoDto> ActualizarCliente(ClienteModel entidad);
        XsDataAccessResponse<List<ClienteModel>> ListarCliente();
    }
}

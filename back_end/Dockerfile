# Descargar la imagen de compilacion de dotnet sdk v3.1
FROM mcr.microsoft.com/dotnet/sdk:5.0-focal AS build-env

# Usamos la carpeta /app para todo lo que sigue
WORKDIR /app

# Empezamos a copiar el .sln del projecto completo
COPY *.sln .

# El codigo del backend, este es usado para que sea posible usar el Dockerfile como formato unico
ARG GRUPO_BACKEND=Proyecto

# Y todos los .csproj de los projectos
COPY ${GRUPO_BACKEND}.Infraestructure.Cross.Cuting/*.csproj   ./${GRUPO_BACKEND}.Infraestructure.Cross.Cuting/
COPY ${GRUPO_BACKEND}.Domain.Entities/*.csproj                ./${GRUPO_BACKEND}.Domain.Entities/
COPY ${GRUPO_BACKEND}.Application.Dto/*.csproj                ./${GRUPO_BACKEND}.Application.Dto/
COPY ${GRUPO_BACKEND}.Domain/*.csproj                         ./${GRUPO_BACKEND}.Domain/
COPY ${GRUPO_BACKEND}.Infraestructure.Data/*.csproj           ./${GRUPO_BACKEND}.Infraestructure.Data/
COPY ${GRUPO_BACKEND}.Application/*.csproj                    ./${GRUPO_BACKEND}.Application/
COPY ${GRUPO_BACKEND}.Distribuited.Services/*.csproj          ./${GRUPO_BACKEND}.Distribuited.Services/

# Y ahora con toda la info de arriba descargamos las dependencias que se necesitan
# (para eso era lo de copiar cada csproj)
RUN dotnet restore

# Copiamos el resto
COPY ${GRUPO_BACKEND}.Infraestructure.Cross.Cuting/.          ./${GRUPO_BACKEND}.Infraestructure.Cross.Cuting/
COPY ${GRUPO_BACKEND}.Domain.Entities/.                       ./${GRUPO_BACKEND}.Domain.Entities/
COPY ${GRUPO_BACKEND}.Application.Dto/.                       ./${GRUPO_BACKEND}.Application.Dto/
COPY ${GRUPO_BACKEND}.Domain/.                                ./${GRUPO_BACKEND}.Domain/
COPY ${GRUPO_BACKEND}.Infraestructure.Data/.                  ./${GRUPO_BACKEND}.Infraestructure.Data/
COPY ${GRUPO_BACKEND}.Application/.                           ./${GRUPO_BACKEND}.Application/
COPY ${GRUPO_BACKEND}.Distribuited.Services/.                 ./${GRUPO_BACKEND}.Distribuited.Services/


# La configuracion que se usa en tiempo de compilacion
RUN dotnet publish -c release -o ./out -r linux-x64 --self-contained true

# Construyendo el container que va a correr la aplicacion (descargando solo el runtime, el sdk es inecesario)
FROM mcr.microsoft.com/dotnet/runtime-deps:5.0-buster-slim-amd64 as deploy

RUN sed -i'.bak' 's/$/ contrib/' /etc/apt/sources.list

RUN apt-get update \
   && apt-get install -y --no-install-recommends libgdiplus libc6-dev ttf-mscorefonts-installer fontconfig \
   && apt-get clean \
   && rm -rf /var/lib/apt/lists/*


EXPOSE 5000
# Como siempre todo va a la carpeta /app
WORKDIR /app

# Copiando desde la fase build-env (la de arriba si ves la linea 2) los archivos necesario para
# ejecutar esta aplicacion
COPY --from=build-env /app/out/ /app

# Y cada vez que el contenedor se ejecute va a correr el dotnet con el dll especifico
CMD ["/app/Proyecto.Distribuited.Services", "--urls", "http://0.0.0.0:5000"]
